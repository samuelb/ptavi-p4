#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
'''SERVER = sys.argv[1]
PORT = int(sys.argv[2])
LINE = ' '.join(sys.argv[3:])'''


def main():
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    LINE = ' '.join(sys.argv[3:])
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
